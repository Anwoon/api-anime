<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180618132309 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anime ADD image_url LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE anime_user CHANGE id_user id_user INT DEFAULT NULL, CHANGE id_anime id_anime INT DEFAULT NULL, CHANGE id_category id_category INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anime DROP image_url');
        $this->addSql('ALTER TABLE anime_user CHANGE id_user id_user INT NOT NULL, CHANGE id_category id_category INT NOT NULL, CHANGE id_anime id_anime INT NOT NULL');
    }
}
