<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnimeUser
 *
 * @ORM\Table(name="anime_user", indexes={@ORM\Index(name="Untitled_fk0", columns={"id_user"}), @ORM\Index(name="Untitled_fk1", columns={"id_category"}), @ORM\Index(name="Untitled_fk2", columns={"id_anime"})})
 * @ORM\Entity(repositoryClass="App\Repository\AnimeUserRepository")
 */
class AnimeUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_category", referencedColumnName="id")
     * })
     */
    private $idCategory;

    /**
     * @var \Anime
     *
     * @ORM\ManyToOne(targetEntity="Anime")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_anime", referencedColumnName="id")
     * })
     */
    private $idAnime;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $episode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdCategory(): ?Category
    {
        return $this->idCategory;
    }

    public function setIdCategory(?Category $idCategory): self
    {
        $this->idCategory = $idCategory;

        return $this;
    }

    public function getIdAnime(): ?Anime
    {
        return $this->idAnime;
    }

    public function setIdAnime(?Anime $idAnime): self
    {
        $this->idAnime = $idAnime;

        return $this;
    }

    public function getEpisode(): ?int
    {
        return $this->episode;
    }

    public function setEpisode(?int $episode): self
    {
        $this->episode = $episode;

        return $this;
    }


}
