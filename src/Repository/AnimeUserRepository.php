<?php

namespace App\Repository;

use App\Entity\AnimeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AnimeUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnimeUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnimeUser[]    findAll()
 * @method AnimeUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimeUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AnimeUser::class);
    }

    public function CountAnimeByCategoryAndUser($id) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT category.name, count(*) AS counter
            FROM anime_user
            INNER JOIN category ON category.id = anime_user.id_category
            WHERE id_user = ".$id."
            GROUP BY id_category
            ORDER BY counter DESC
            ";

        $request = $conn->prepare($sql);
        $request->execute();

        return $request->fetchAll();
    }


//    /**
//     * @return AnimeUser[] Returns an array of AnimeUser objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnimeUser
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
