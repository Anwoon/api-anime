<?php

namespace App\Repository;

use App\Entity\Anime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Anime|null find($id, $lockMode = null, $lockVersion = null)
 * @method Anime|null findOneBy(array $criteria, array $orderBy = null)
 * @method Anime[]    findAll()
 * @method Anime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Anime::class);
    }

    public function findByUser($id_user) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT a.*, c.name as category, au.id as relation FROM anime as a
            INNER JOIN anime_user as au
            ON au.id_anime = a.id
            INNER JOIN user as u
            ON u.id = au.id_user
            INNER JOIN category as c
            ON c.id = au.id_category
            WHERE u.id = :id
            ";

        $request = $conn->prepare($sql);
        $request->execute(['id' => $id_user]);

        return $request->fetchAll();
    }

    public function findOneByUser($id_user, $id ) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT a.*, c.name as category, au.id as relation, au.episode as view_episode FROM anime as a
            INNER JOIN anime_user as au
            ON au.id_anime = a.id
            INNER JOIN user as u
            ON u.id = au.id_user
            INNER JOIN category as c
            ON c.id = au.id_category
            WHERE u.id = :iduser AND
            a.id = :id
            ";

        $request = $conn->prepare($sql);
        $request->execute(['iduser' => $id_user, 'id' => $id]);

        return $request->fetchAll();
    }

    public function deleteOneByUser($id_anime,$id_user) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            DELETE FROM anime_user WHERE id_user = ".$id_user." AND id_anime = ".$id_anime."
            ";

        $request = $conn->prepare($sql);
        $request->execute();
    }

    public function search($name) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT * FROM anime as a WHERE a.name LIKE '%".$name."%'
            ";

        $request = $conn->prepare($sql);
        $request->execute();
        return $request->fetchAll();
    }

//    /**
//     * @return Anime[] Returns an array of Anime objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Anime
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
