<?php

namespace App\Controller;

use App\Entity\AnimeUser;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/user/register", name="user_register", methods="POST")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        if(!empty($request->getContent())) {
            $data = json_decode($request->getContent(), true);
            $entityManager = $this->getDoctrine()->getManager();

            $users = $this->getDoctrine()->getRepository(User::class)->findAll();

            foreach ($users as $value) {
                if ($value->getUsername() === $data['username']) {
                    return new JsonResponse(['response' => 'false', 'message' => 'Nom de compte déja existant']);
                }
            }

            $user = new User();
            $user->setUsername($data['username']);
            $encoded = $encoder->encodePassword($user, $data['password']);
            $user->setPassword($encoded);
            $user->setEmail($data['email']);

            $entityManager->persist($user);
            $entityManager->flush();

            if ($user->getId() != NULL) {
                $data = ['response' => 'true', 'message' => 'Compte créer avec succés'];
            } else {
                $data = ['response' => 'false', 'message' => 'Error'];
            }

            return new JsonResponse($data);
        }
        return new JsonResponse(['']);
    }

    /**
     * @Route("/user/connect", name="user_login" , methods="POST")
     */
    public function connect(Request $request, UserPasswordEncoderInterface $encoder){
        if(!empty($request->getContent())) {
            $data = json_decode($request->getContent(), true);
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
                'username' => $data['username'],
            ]);
            if ($user !== NULL) {
                $encoded = $encoder->isPasswordValid($user, $data['password']);
            }
        }
        if(empty($data['username']) || $user === NULL) {
            return new JsonResponse(['response' => false,'message' => 'Nom de compte ou compte inexistant, veuillez réessayer ultérieurement']);
        } elseif (!$encoded) {
            return new JsonResponse(['response' => false, 'message' => 'Mot de passe incorrecte, veuillez réessayer ultérieurement']);
        }
        else {
            return new JsonResponse(['response' => true, 'user_id' => $user->getId(), 'user_name' => $user->getUsername()]);
        }

    }

//    /**
//     * @Route("/user/{id}", name="profil", methods="GET")
//     */
//    public function profil($id) {
//        $data = $this->getDoctrine()->getRepository(User::class)->find($id);
//        $data = $this->serializer->serialize($data, 'json');
//        return new Response($data);
//    }

    /**
     * @Route("/user/profil/{id}", name="profil_one", methods="GET")
     */
    public function oneProfil($id) {
        $data = $this->getDoctrine()->getRepository(User::class)->find($id);

        $count = $this->getDoctrine()->getRepository(AnimeUser::class)->CountAnimeByCategoryAndUser($id);
        $array = [];
        $array['username'] = $data->getUsername();
        $array['email'] = $data->getEmail();
        $array['password'] = $data->getPassword();
        $array['id'] = $data->getId();
        $array['count'] = $count;

        $data = $this->serializer->serialize($array, 'json');

        return new Response($data);
    }

    /**
     * @Route("/user/profil/{id}", name="profil_edit", methods="POST")
     */
    public function update($id, Request $request, UserPasswordEncoderInterface $encoder) {
        $em = $this->getDoctrine()->getManager();
        $data = $this->getDoctrine()->getRepository(User::class)->find($id);
        if(!empty($data)) {
            $datas = json_decode($request->getContent(), true);
            if (!empty($datas['username'])) {
                $usernameCheck = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $datas['username']]);
                if ($usernameCheck === null) {
                    $data->setUsername($datas['username']);
                    $em->flush();
                    return new JsonResponse(['message' => 'Pseudo Modifier']);
                } else {
                    return new JsonResponse(['message' => 'Nom de compte déja utiliser']);
                }
            }

            if (!empty($datas['password'])) {
                $encoded = $encoder->encodePassword($data, $datas['password']);
                $data->setPassword($encoded);
                $em->flush();
                return new JsonResponse(['message' => 'Mot de Passe Modifier']);
            }

            if (!empty($datas['email'])) {
                $data->setEmail($datas['email']);
                $em->flush();
                return new JsonResponse(['message' => 'Email Modifier']);
            }
        } else {
            return new JsonResponse(['message' => 'Utilisateur non existant']);
        }
    }

    /**
     * @Route("/user/recovery", name="recovery_password", methods="POST")
     */
    public function RecoveryPassword(Request $request, \Swift_Mailer $mailer, UserPasswordEncoderInterface $encoder){
        $data = json_decode($request->getContent(), true);
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $data['email']]);
        if(!empty($user)) {
            $em = $this->getDoctrine()->getManager();
            $new_password = $user->generateRandomString(6);
            $encoded = $encoder->encodePassword($user, $new_password);
            $user->setPassword($encoded);
            $em->flush();

            $message = (new \Swift_Message('Récupération de votre Mot de Passe Anime Dreams'))
                ->setFrom('send@example')
                ->setTo($data['email'])
                ->setBody(
                    $this->renderView(
                        'email/recoverypassword.html.twig',
                        array('password' => $new_password, 'username' => $user->getUsername())
                    ),
                    'text/html'
            );
            $mailer->send($message);
            return new JsonResponse(['message' =>'Un email a été envoyer avec votre nouveau de passe a l\'adresse suivante: '.$data['email']]);
        } else {
            return new JsonResponse(['message' => 'L\'email entrer n\'est pas présent dans notre base de donnée']);
        }
    }
}
