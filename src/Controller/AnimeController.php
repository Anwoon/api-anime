<?php

namespace App\Controller;

use App\Entity\Anime;
use App\Entity\AnimeUser;
use App\Entity\Category;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Jikan\Jikan;

class AnimeController extends Controller
{

    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/animelist/add/{id_user}/{id_anime}/{id_category}", name="anime_add", methods="POST")
     */
    public function add($id_user, $id_anime, $id_category)
    {
        $animecheck = $this->getDoctrine()->getRepository(AnimeUser::class)->findOneBy(['idUser' => $id_user, 'idAnime' => $id_anime]);

        if($animecheck === null){

        $em = $this->getDoctrine()->getManager();

        $anime = $this->getDoctrine()->getRepository(Anime::class)->findOneBy(['id' => $id_anime]);
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id_user]);
        $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['id' => $id_category]);


        $anime_user = new AnimeUser();
        $anime_user->setIdAnime($anime);
        $anime_user->setIdCategory($category);
        $anime_user->setIdUser($user);
        if($id_category = 4) {
            $anime_user->setEpisode($anime->getEpisode());
        } else {
            $anime_user->setEpisode(0);
        }

        $em->persist($anime_user);

        $em->flush();

        return new JsonResponse(['response' => 'true', 'message' => 'Anime bien ajoutée']);
        } else {

        return new JsonResponse(['response' => 'false', 'message' => 'Anime déjà présent dans votre liste']);
        }
    }

    /**
     * @Route("/animelist/{id}", name="myanime_list", methods="GET")
     *      id is id user
     */
    public function home($id)
    {
        $data = $this->getDoctrine()->getRepository(Anime::class)->findByUser($id);

        return new JsonResponse($data);
    }

    /**
     * @Route("/animelist", name="anime_list", methods="GET")
     *      id is id user
     */
    public function animelist()
    {
        $data = $this->getDoctrine()->getRepository(Anime::class)->findBy([],['name' =>  'ASC']);
        $data = $this->serializer->serialize($data, 'json');
        return new Response($data);
    }

    /**
     * @Route("/animelist/{id}", name="anime_delete", methods="DELETE")
     *      id is relation
     */
    public function delete($id)
    {
        $data = $this->getDoctrine()->getRepository(AnimeUser::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($data);
        $em->flush();

        return new JsonResponse(['message' => 'Anime supprimer de votre liste']);
    }

    /**
     * @Route("/anime/{id}", name="anime_show", methods="GET")
     *   id is anime id
     */
    public function show($id, Jikan $jikan)
    {
        $data = $this->getDoctrine()->getRepository(Anime::class)->findOneBy(['id' => $id]);

        if($data->getImageUrl() == NULL) {
            $em = $this->getDoctrine()->getManager();
            $id = intval($data->getId());
            $image = $jikan->Anime($id)->response['image_url'];
            $data->setImageUrl($image);
            $em->flush();
        }

        $data = $this->serializer->serialize($data, 'json');
        return new Response($data);
    }

    /**
     *  @Route("/animelist/update/{id}", name="anime_update", methods="POST")
     */
    public function update($id,Request $request) {
        $em = $this->getDoctrine()->getManager();
        $data = $this->getDoctrine()->getRepository(AnimeUser::class)->find($id);
        if(!empty($request->getContent())) {
            $datas = json_decode($request->getContent(), true);
            $category = $this->getDoctrine()->getRepository(Category::class)->find($datas['id_category']);
            $data->setIdCategory($category);
        }

        $em->flush();

        return new JsonResponse(['response' => 'true', 'message' => 'Anime Modifier']);
    }

    /**
     * @Route("/animelist/update/{id}/{episode}", name="anime_episode", methods="GET")
     */
    public function episode($id, $episode) {

        $em = $this->getDoctrine()->getManager();
        $data = $this->getDoctrine()->getRepository(AnimeUser::class)->find($id);
        $data->setEpisode($episode);

        $em->flush();

        return new JsonResponse(['response' => 'true', 'message' => 'Anime Modifier']);
    }

    /**
     * @Route("/animelist/search/{name}", name="myanime_list_search", methods="GET")
     */
    public function search($name) {
        $name = str_replace("%20"," ",$name);
        $data = $this->getDoctrine()->getRepository(Anime::class)->search($name);
        return new JsonResponse($data);
    }

    /**
     * @Route("/animelist/{id_user}/{id_anime}", name="myanime_list_one", methods="GET")
     */
    public function showUserAnime($id_user, $id_anime)
    {
        $data = $this->getDoctrine()->getRepository(Anime::class)->findOneByUser($id_user, $id_anime);

        return new JsonResponse($data);
    }

}
